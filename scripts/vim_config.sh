#!/bin/bash

# running vim in silent mode to launch some commands
/usr/bin/vim -e +PluginUpdate +qall >>/dev/null 2>&1
